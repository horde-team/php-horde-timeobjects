php-horde-timeobjects (2.1.4-6) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 12:51:38 +0200

php-horde-timeobjects (2.1.4-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959323).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/copyright: Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 13 May 2020 21:18:27 +0200

php-horde-timeobjects (2.1.4-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:44:04 +0200

php-horde-timeobjects (2.1.4-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 16:41:47 +0200

php-horde-timeobjects (2.1.4-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Fri, 06 Apr 2018 13:18:16 +0200

php-horde-timeobjects (2.1.4-1) unstable; urgency=medium

  * New upstream version 2.1.4

 -- Mathieu Parent <sathieu@debian.org>  Wed, 27 Sep 2017 21:48:14 +0200

php-horde-timeobjects (2.1.3-1) unstable; urgency=medium

  * New upstream version 2.1.3

 -- Mathieu Parent <sathieu@debian.org>  Sun, 18 Dec 2016 22:58:25 +0100

php-horde-timeobjects (2.1.2-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Thu, 09 Jun 2016 21:01:42 +0200

php-horde-timeobjects (2.1.2-1) unstable; urgency=medium

  * New upstream version 2.1.2

 -- Mathieu Parent <sathieu@debian.org>  Wed, 06 Apr 2016 08:41:44 +0200

php-horde-timeobjects (2.1.1-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 21:05:17 +0100

php-horde-timeobjects (2.1.1-1) unstable; urgency=medium

  * New upstream version 2.1.1

 -- Mathieu Parent <sathieu@debian.org>  Sat, 06 Feb 2016 22:23:38 +0100

php-horde-timeobjects (2.1.0-5) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 07:46:07 +0200

php-horde-timeobjects (2.1.0-4) unstable; urgency=medium

  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 01:27:17 +0200

php-horde-timeobjects (2.1.0-3) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 May 2015 09:12:03 +0200

php-horde-timeobjects (2.1.0-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb

 -- Mathieu Parent <sathieu@debian.org>  Wed, 27 Aug 2014 07:45:03 +0200

php-horde-timeobjects (2.1.0-1) unstable; urgency=medium

  * New upstream version 2.1.0

 -- Mathieu Parent <sathieu@debian.org>  Sat, 17 May 2014 13:22:35 +0200

php-horde-timeobjects (2.0.4-1) unstable; urgency=low

  * New upstream version 2.0.4

 -- Mathieu Parent <sathieu@debian.org>  Mon, 24 Jun 2013 11:26:39 +0200

php-horde-timeobjects (2.0.3-2) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Wed, 12 Jun 2013 21:37:57 +0200

php-horde-timeobjects (2.0.3-1) unstable; urgency=low

  * New upstream version 2.0.3

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 17:51:07 +0200

php-horde-timeobjects (2.0.2-1) unstable; urgency=low

  * New upstream version 2.0.2

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 22:55:32 +0100

php-horde-timeobjects (2.0.1-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 21:15:48 +0100

php-horde-timeobjects (2.0.1-1) unstable; urgency=low

  * timeobjects package
  * Initial packaging (Closes: #657384)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Sat, 01 Dec 2012 11:45:17 +0100
